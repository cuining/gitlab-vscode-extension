import { browser } from '@wdio/globals';

/**
 * Waits for the code suggestion icon to switch from loading back to enabled.
 * Used by verifyCodeSuggestion()
 *
 * @async
 * @param {Object} statusBar - Status bar object. It is passed for speed so we don't miss the loading icon.
 * @returns {Promise<void>}
 */
const waitForDuoSuggestion = async statusBar => {
  await browser.waitUntil(
    async () => {
      const items = await statusBar.getItems();

      return await items.find(i =>
        i.includes('gitlab-code-suggestions-loading, Code suggestion is loading...'),
      );
    },
    {
      timeoutMsg: `Code suggestions loading icon did not appear.`,
    },
  );
  await browser.waitUntil(
    async () => {
      const items = await statusBar.getItems();

      return await items.find(i =>
        i.includes('gitlab-code-suggestions-enabled, Code suggestions are enabled'),
      );
    },
    {
      timeout: 10000,
      timeoutMsg: `Code suggestions did not finish.`,
    },
  );
};

/**
 * Waits for a code suggestion to generate and contain expected text.
 *
 * @async
 * @param {Object} tab - The text editor window
 * @param {Object} statusBar - The status bar object
 * @param {string} initialPrompt - The initial prompt text
 * @returns {Promise<void>}
 */
const verifyCodeSuggestion = async (tab, statusBar, initialPrompt) => {
  await waitForDuoSuggestion(statusBar);
  await browser.keys('Tab');

  const editorText = await tab.getText();
  const generatedCode = editorText.replace(initialPrompt, '');

  await expect(generatedCode).not.toContain(initialPrompt);
  await expect(generatedCode.length).toBeGreaterThan(1, {
    message: `Could not find generated code for prompt ${initialPrompt}.`,
  });
};

export { verifyCodeSuggestion };
