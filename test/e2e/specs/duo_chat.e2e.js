import { browser } from '@wdio/globals';
import { completeAuth, verifyDuoChatResponse, askDuoChat } from '../helpers/index.js';

describe('GitLab Workflow Extension Duo Chat', async () => {
  let workbench;

  before(async () => {
    await completeAuth();
  });

  beforeEach(async () => {
    workbench = await browser.getWorkbench();
    await workbench.executeCommand('GitLab Duo Chat: Start a new conversation');
    const duoChatWebView = await workbench.getWebviewByTitle('GitLab Duo Chat');

    await duoChatWebView.open();
  });

  it('sends a Duo chat request and receives a response', async () => {
    await askDuoChat('/clear');

    await askDuoChat('hi');
    const expectedOutput = 'GitLab';
    await verifyDuoChatResponse(expectedOutput);
  });
});
