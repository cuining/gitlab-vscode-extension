import {
  TELEMETRY_NOTIFICATION,
  TRACKING_EVENTS,
  CODE_SUGGESTIONS_CATEGORY,
} from '@gitlab-org/gitlab-lsp';
import { BaseLanguageClient } from 'vscode-languageclient';
import { codeSuggestionStreamAccepted } from './code_suggestion_stream_accepted';
import { CompletionStream } from '../../language_server/completion_stream';
import { createFakePartial } from '../../test_utils/create_fake_partial';

describe('codeSuggestionStreamAccepted command', () => {
  const stream = createFakePartial<CompletionStream>({
    trackingId: 'uniqueTrackingId',
    cancel: jest.fn(),
  });

  const languageClient = createFakePartial<BaseLanguageClient>({
    sendNotification: jest.fn(),
  });
  const runCommand = codeSuggestionStreamAccepted(languageClient);

  beforeEach(async () => {
    await runCommand(stream);
  });

  it('should cancel the stream', () => {
    expect(stream.cancel).toHaveBeenCalled();
  });

  it('should send telemetry notification', () => {
    expect(languageClient.sendNotification).toHaveBeenCalledWith(TELEMETRY_NOTIFICATION, {
      action: TRACKING_EVENTS.ACCEPTED,
      category: CODE_SUGGESTIONS_CATEGORY,
      context: {
        trackingId: stream.trackingId,
      },
    });
  });
});
