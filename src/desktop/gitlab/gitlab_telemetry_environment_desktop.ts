import * as vscode from 'vscode';
import { GitLabTelemetryEnvironment } from '../../common/platform/gitlab_telemetry_environment';

export class GitLabTelemetryEnvironmentDesktop implements GitLabTelemetryEnvironment {
  #subscriptions: vscode.Disposable[] = [];

  constructor() {
    this.#emitTelemetryStatus(vscode.env.isTelemetryEnabled);

    this.#subscriptions.push(
      vscode.env.onDidChangeTelemetryEnabled(enabled => this.#emitTelemetryStatus(enabled)),
    );
  }

  // eslint-disable-next-line class-methods-use-this
  isTelemetryEnabled(): boolean {
    return vscode.env.isTelemetryEnabled;
  }

  get onDidChangeTelemetryEnabled() {
    return this.#onDidChangeTelemetryEnabled.event;
  }

  #onDidChangeTelemetryEnabled: vscode.EventEmitter<boolean> = new vscode.EventEmitter<boolean>();

  #emitTelemetryStatus(isEnabled: boolean) {
    this.#onDidChangeTelemetryEnabled.fire(isEnabled);
  }

  dispose() {
    this.#subscriptions.forEach(s => s.dispose());
  }
}
